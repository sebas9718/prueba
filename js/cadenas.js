var firstName = "Juan";
var lastName = "Martinez";

console.log(firstName, lastName);

var phrase1 = "Este es el \" caracter";
console.log(phrase1);
/*
\n
\t
*/

var phrase2 = `caracter = ' y caracter = "`;
console.log(phrase2);

var theMovie = "Star " + "Wars";
console.log(theMovie);
theMovie += ": The return of the jedi";
console.log(theMovie);
var year = 1983;
theMovie += " - " + year;
console.log(theMovie);

var length = theMovie.length;
console.log(length)

var letter = theMovie[0];
console.log(letter);

