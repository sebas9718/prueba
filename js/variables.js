console.log("Hello form another file")
/*
data types:
undefined, null, number, string, boolean, objects
*/
var myName = "John"
console.log(myName)

let myCity = "Medellin"
console.log(myCity)

//constantes
const PI = 3.1416;
console.log(PI)

var a, b, c;
a = 3;
b = a;
c = a + b;

console.log(a, b, c);

var sum = 10 + 10;
sum++;
sum += 4;
console.log(sum)

var divide = 10.5 / 5;
divide /= 2;
console.log(divide)
