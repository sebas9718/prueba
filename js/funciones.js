//functions

function myFunction(){
    console.log("This is my function");
}

myFunction();

function sum(num1, num2){
    return num1 + num2;
}

var result = sum(4, 3);
console.log(result);

function incrementValue(value, increment = 1){
    return value + increment;
}
console.log(incrementValue(5, 2));
console.log(incrementValue(10));