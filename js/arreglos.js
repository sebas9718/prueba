console.log("Arreglos")

var elements = ["Cata", 18, "Cardozo"];
console.log(elements)

var element = elements[0];
console.log(element);

elements[1] = 20;
console.log(elements);
elements.push(200);
console.log(elements)
/*
pop:elimina final
shift: elimina primero
unshift: agrega primero
*/

var elements2 = [...elements];
elements2.push("Colombia")
console.log(elements2);
console.log(elements);

var matrix = [["A", "B", "C"], ["D", "E", "F"], ["G", "H", "I"]];
console.log(matrix)
console.log(matrix[0][1]);
matrix.push(["J", "K", "L"]);
console.log(matrix);