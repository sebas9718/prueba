function agregar(lista, elemento){
    lista.push(elemento);
}

let elementos = [1,2,3,4,5];
console.log(elementos);
let num = 10;

agregar(elementos, num);
console.log(elementos);

function mensaje(){
    console.log("clasico");
}

const mensaje_arrow = () => console.log("arrow")

mensaje()
mensaje_arrow()